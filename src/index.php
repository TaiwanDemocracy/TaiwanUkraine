<?php $body = <<<EOF


<div class="translated-article">
<span lang="uk"><i class="ua flag"></i><em>Останнє оновлення: <strong>21 квітня 2022 року</strong></em></span>
<span lang="tw"><i class="tw flag"></i><em>最後更新: <strong>2022 年 5 月 09 日</strong></em></span>
<span lang="en"><i class="us flag"></i><em>Last update: <strong>2022 May 9th</strong></em></span>
<span lang="fr"><i class="fr flag"></i><em>Dernière mise à jour: <strong>21 April 2022</strong></em></span>
</div>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw"><em>如果您對本網站的內容和設計有任何想法，<a href="#participate">請告訴我們您的想法</a>。
<br>此外，本網站的完整代碼是<a href="#participate">開放源碼的，並在 Codeberg 上發布</a>，因此任何人都可以為它做出貢獻。</em>
</div>
<div lang="en">
<em>If you have ideas on how to improve the content and design of this web site, <a href="#participate">let us know what you think</a>.
<br>Also, the complete code of this web site is <a href="#participate">open source and published at Codeberg</a> so that anyone can contribute to it.
</em>
</div>
<div lang="fr">
</div>
</div>

<img class="banner" src="/img/sewing-ukraine-wall.jpg">

<h1 id="top" class="translated-article">
<span lang="uk">Тайвань - Україна</span>
<span lang="tw">台灣 - 烏克蘭</span>
<span lang="en">Taiwan - Ukraine</span>
<span lang="fr">Taïwan - Ukraine</span>
</h1>

<div class="translated-article">
<span lang="uk">
Україна спустошена війною.
Україна бореться за збереження своєї територіальної цілісності, своєї свободи та демократії.
Тайвань розуміє, що вона відчуває, як перебуваючи під загрозою, боротися за розвиток та захист власної демократії.
Тайванці вже виявили велику щедрість щодо України, жертвуючи великі суми грошей.
Проте цього недостатньо. Сума збитків в Україні вражає.
Тайвань має продовжувати моральну та фінансову підтримку України.
</span>
<span lang="tw">
烏克蘭正在被<strong class="red">戰爭</strong>摧殘。
<strong>烏克蘭</strong>正在努力維護其<strong>領土完整</strong>、<strong>自由</strong>和<strong>民主</strong>，
台灣了解受到威脅的感覺，以及努力發展民主和保護民主的感覺。
台灣人對烏克蘭很慷慨，捐贈了很多的金錢，然而這樣還不夠。
烏克蘭的破壞是驚人的，<strong>台灣必須繼續為烏克蘭提供道義和金錢上的支持</strong>。
</span>
<span lang="en">Ukraine is being devastated by the <strong class="red">war</strong>.
<strong>Ukraine</strong> is fighting to preserve its <strong>territorial integrity</strong>, its <strong>freedom</strong> and its <strong>democracy</strong>.
Taiwan understands what it feels to be under threat, to struggle to develop and protect its own democracy.
The Taiwanese people have already shown a lot of generosity towards Ukraine, donating large amounts of money.
However it is not enough.
The amount of damage in Ukraine is staggering.
<strong>Taiwan must continue its moral and financial support of Ukraine.</strong>
</span>
<span lang="fr">
L'Ukraine est en train d'être dévastée par la guerre.
L'Ukraine se bat pour préserver son intégrité territoriale, sa liberté et sa démocratie.
Taiwan comprend ce que c'est d'être menacé, de se battre pour développer et protéger sa propre démocratie.
Les Taïwanais se sont déjà montrés très généreux envers l'Ukraine en donant de grandes quantités d'argent.
Cependant, ce n'est pas assez.
L'étendu des dégâts en Ukraine est terrible.
Taiwan doit continuer à soutenir l'Ukraine moralement et finiancièrement.
</span>
</div>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw"><p>這場對烏克蘭的侵略戰爭是因為一位<strong class="red">俄羅斯獨裁者</strong> <strong class="red">熱愛權力</strong>（Love of Power）而開始的。</p>
<p>在<strong>愛的力量</strong>（Power of Love）推動下，<strong>世界</strong> <strong>團結</strong>起來支持烏克蘭。
<strong>世界上所有最偉大的民主國家</strong>都衡量了這場衝突中的利害關係的重要性。
我們不能讓專制政權不斷威脅世界和平。
在整個人類歷史上，兩個民主國家之間從未發生過戰爭。
戰爭絕大多是由試圖擴大其勢力範圍和領土控制的獨裁政權和帝國發起的。
當民主國家捲入戰爭時，總是出於自衛保護自身的立場下。
烏克蘭將贏得這場戰爭，因為它背後的世界是團結的。
一個和平的新世界秩序必須從這場戰爭中產生。
如果<strong>愛</strong>與<strong>團結</strong>攜手同行，那麼一切皆有可能，<strong>和平</strong>也能恢復。
</p>
</div>
<div lang="en">
<p>This war of aggression against Ukraine started because a <strong class="red">Russian dictator</strong> had <strong class="red">Love for Power</strong>.</p>
<p><strong>The world</strong> is <strong>united</strong> in its support of Ukraine, animated by the <strong>Power of Love</strong>.
All of <strong>the world's greatest democracies</strong> have measured the importance of what was at stake in this conflict.
We cannot let authoritarian regimes constantly threaten world peace.
In all of the humanity's history, there has never been a war between two democracies.
Wars are initiated by dictatorial regimes and empires trying to extend their spheres of influence and territorial control.
And when democracies get involved in wars, it is always in self defence.
Ukraine will win this war, because the world is united behind it.
A peaceful new world order must emerge from this war.
If <strong>Love</strong> and <strong>Solidarity</strong> walk hand in hand, then everything is possible and <strong>Peace</strong> can be restored.
</p>
</div>
<div lang="fr">
</div>
</div>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw">
在俄羅斯入侵烏克蘭之前，沒有人能夠預料到西方的反應會是怎樣。
每個人都對世界上最偉大的民主國家如此團結和有力的反應感到驚訝。
我們相信烏克蘭最終會贏得這場戰爭，因為所有那些重視和平與民主的國家都在積極支持烏克蘭。
整個歐盟在支持烏克蘭。
整個北約在支持烏克蘭。
<strong>世界上所有最偉大的民主國家在支持烏克蘭。</strong>
更重要的是，這些支持不僅僅是像徵性的，不僅僅是美麗的文字和虔誠的意圖。
所有這些國家都在跳出自己的舒適區，為烏克蘭提供所需的實際幫助做出犧牲。
<ul>
<li><strong>波蘭</strong>全體人民打開了家門、心門和錢包，歡迎數以百萬的烏克蘭難民。</li>
<li><strong>立陶宛</strong>可能是一個小國，但它再次站在了歷史正確的一邊。 去年，立陶宛讓台灣在維爾紐斯開設代表處，贏得了台灣民眾的心，勇敢地抵制了中國的經濟勒索。 今天再一次，勇敢的立陶宛又是最有勇氣地全力支持烏克蘭的國家之一，以其最珍視的民主價值觀的名義全力支持烏克蘭。</li>
<li><strong>德國</strong>已經重提恢復了自二戰結束以來一直堅持的政策，以便為烏克蘭提供所需的軍事支持。</li>
<li><strong>美國</strong>剛剛為烏克蘭制定了與它在 1941 年二戰初期制定的同樣果敢而影響深遠的立法，以確保民主戰勝極權主義政權。</li>
<li>列舉...</li>
</ul>
如果我們要詳細說明每個國家為確保烏克蘭獲勝而實際採取的所有措施，那麼這份清單將會很長。
烏克蘭將獲勝，因為<strong>熱愛和平的民主國家團結一致，齊心協力</strong>。
</div>
<div lang="en">
Before the Russian invasion of Ukraine, no one could have predicted how the Western response would be.
Everybody was surprised at how united and forceful the response from the world's greatest democracy was.
We are confident that Ukraine will eventually win this war because all those countries that value peace and democracy are actively supporting Ukraine.
The whole of the European Union is supporting Ukraine.
The whole of NATO is supporting Ukraine.
<strong>All of the world's greatest democracies are supporting Ukraine.</strong>
What's more, the support is not only symbolic, not only beautiful words and pious intentions.
All of these countries are going beyond their comfort zone, making sacrifices in order to provide Ukraine with the practical help it needs.
<ul>
<li>The whole of the population of <strong>Poland</strong> opened their homes, their hearts and their wallets to welcome millions of Ukrainian refugees.</li>
<li><strong>Lithuania</strong> may be a tiny country, but once again it stands on the right side of history.
Last year, Lithuania won the hearts of the Taiwanese people for letting Taiwan open a representative office in Vilnius,
and bravely resisted the Chinese economic blackmail.
Today again, brave Lithuania is one of the countries who most courageously put everything it has to support Ukraine, in the name of their most cherished democratic values.</li>
<li><strong>Germany</strong> has reverted policies it closely held since the end of World War II in order to provide Ukraine with the military support it needed.</li>
<li>The <strong>USA</strong> has just enacted for Ukraine the same kind of bold and far-reaching legislation it enacted in 1941, at the beginning of World War II, in order to secure the victory of democracy against totalitarian regimes.</li>
<li>Etc...</li>
</ul>
The list would be very long, if we were to detail all of which each and every country is practically doing to secure Ukraine's victory.
Ukraine will win because <strong>peace loving democracies are all united</strong>, working in concert.
</div>
<div lang="fr">
</div>
</div>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw">
<strong>台灣</strong>的幫助至少必須與民主世界其他國家一樣<strong>堅決</strong>。
台灣已經慷慨捐款，但這裡的利害關係太重要了。
台灣，它的政府和它的人民，必須加倍努力，使我們的幫助達到其他國家的水平。
贏得和平與民主不是一個組織或一個網站的工作。
台灣社會各個層面都必須參與，包括中央政府、地方政府、慈善組織、宗教組織、媒體機構、學校、企業等。
</div>
<div lang="en">
<strong>Taiwan</strong>'s help must at least be as <strong>resolute</strong> as that of the rest of the democratic world.
Taiwan has already generously donated, but what is at stake here is too important.
Taiwan, its government and its population, must increase their effort so that our help reaches the level of that of other countries.
Winning peace and democracy cannot be the work of a single organization, or of a single web site.
All levels of the Taiwanese society must be involved, including the central government, local governments, charitable organizations, religious organizations, media organizations, schools, businesses, etc.
</div>
<div lang="fr">
</div>
</div>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw"><p>如果有一天我們取得了重大成就，那將是<strong>團隊合作</strong>的結果。
<strong>愛的力量</strong>與<strong>團隊合作的力量</strong>相結合，可以成就偉大的事業。</p>
<p>本網站的內容和設計是完全<a href="#participate">開放源碼</a>的。
任何有<strong>想法</strong>和<strong>技能</strong>的人都可以為它<a href="#participate">做出貢獻</a>。
目標是為創建信息最豐富的網站，以確保整個台灣社會能夠發光，並為烏克蘭提供其需要和應得的道義和金錢支持。</p>
<p><a href="#participate">加入我們</a>並成為團隊的一員！</p>
</div>
<div lang="en">
<p>If we ever achieve anything significant one day, it will be the result of <strong>teamwork</strong>.
The <strong>Power of Love</strong> combined with the <strong>Strength of Teamwork</strong> can achieve great things.</p>
<p>The content and the design of this web site is completely <a href="#participate">open source</a>.
Anyone with <strong>ideas</strong> and with <strong>skills</strong> can <a href="#participate">contribute</a> to it.
The goal is to create the most informative web site to ensure that the whole of the Taiwanese society can shine and provide Ukraine with the moral and financial support it needs and deserves.</p>
<p><a href="#participate">Join us</a> and be part of the team!</p>
</div>
<div lang="fr">
</div>
</div>

<h2 class="translated-article">
<span lang="uk"></span>
<span lang="tw">目錄</span>
<span lang="en">Menu</span>
<span lang="fr"></span>
</h2>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw">
	<ul>
	<li><a href="#top">導論</a></li>
	<li><a href="#rebuild">重建</a></li>
	<li><a href="#donate">捐款</a>
		<ul>
		<li><a href="#donate-today">今天捐款</a></li>
		<li><a href="#long-term-support-project">長期支持計劃</a></li>
		<li><a href="#donate-other">其他捐贈方式</a></li>
		</ul>
	</li>
	<li><a href="#events">活動</a>
		<ul>
		<li><a href="#may-8-2022-rally">5月8日烏克蘭邁向勝利集會</a></li>
		<li><a href="#ukraine-art-exhibition">烏克蘭藝術台中畫展</a></li>
		</ul>
	</li>
	<li><a href="#participate">參加</a></li>
	<li><a href="#development">本網站開發</a></li>
	</ul>
</div>
<div lang="en">
	<ul>
	<li><a href="#top">Introduction</a></li>
	<li><a href="#rebuild">Rebuild</a></li>
	<li><a href="#donate">Donate</a>
		<ul>
		<li><a href="#donate-today">Donate today</a></li>
		<li><a href="#long-term-support-project">Long term support project</a></li>
		<li><a href="#donate-other">Other places to donate</a></li>
		</ul>
	</li>
	<li><a href="#events">Events</a>
		<ul>
		<li><a href="#may-8-2022-rally">May 8th Ukraine Towards Victory Rally</a></li>
		<li><a href="#ukraine-art-exhibition">Ukraine art exhibition in Taichung</a></li>
		</ul>
	</li>
	<li><a href="#participate">Participate</a></li>
	<li><a href="#development">Web site development</a></li>
	</ul>
</div>
<div lang="fr">
</div>
</div>

<h2 id="rebuild" class="translated-article">
<span lang="uk">Перебудувати</span>
<span lang="tw">重建</span>
<span lang="en">Rebuild</span>
<span lang="fr">Reconstruire</span>
</h2>

<div class="translated-article">
<div lang="uk">
Тайвань може допомогти Україні такими способами:
	<ul>
	<li>Надавати фінансову допомогу на невідкладні гуманітарні та медичні потреби.</li>
	<li>Після війни допомагайте відновлювати школи, лікарні та іншу важливу інфраструктуру.</li>
	</ul>
</div>
<div lang="tw">
台灣可以透過以下方式幫助烏克蘭：
	<ul>
	<li>提供財務救助，基於人道主義和醫療需求出發點。</li>
	<li>幫助重建學校、醫院等重要基礎設施，在戰後。</li>
	</ul>
</div>
<div lang="en">
Taiwan can help Ukraine in the following ways:
	<ul>
	<li>Provide financial help for immediate humanitarian and medical needs.</li>
	<li>After the war, help rebuild schools, hospitals and other important infrastructure.</li>
	</ul>
</div>
<div lang="fr">
Taïwan peut aider l'Ukraine des manières suivantes:
	<ul>
	<li>Fournir une aide financière pour les besoins humanitaires et médicaux immédiats.</li>
	<li>Après la guerre, aidez à reconstruire des écoles, des hôpitaux et d'autres infrastructures importantes.</li>
	</ul>
</div>
</div>


<h2 id="donate" class="translated-article">
<span lang="uk">пожертвувати</span>
<span lang="tw">捐款</span>
<span lang="en">Donate</span>
<span lang="fr">Donner</span>
</h2>

<h3 id="donate-today" class="translated-article">
<span lang="uk"></span>
<span lang="tw">今天捐款</span>
<span lang="en">Donate today</span>
<span lang="fr"></span>
</h3>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw">
<p>
如果您今天想捐款，我們的朋友在 <a href="https://www.supportukraine.tw/">supportukraine.tw</a> 網站上已經整理了一份不錯的慈善機構和烏克蘭組織名單，供您捐款。
</p>
<p>
好的是，每個組織都提供了他們的信息來源，以證明它是一個合法的組織，以確保捐贈真的去了應該去的地方。 在可能需要的地方，他們還提供了帶有中文註釋的烏克蘭語表格的屏幕截圖。
</p>
<p>
如果您碰巧使用 Facebook，請在那裡查看他們的群組。 他們非常積極，他們正在努力爭取台灣對烏克蘭的支持。
</p>
</div>
<div lang="en">
<p>If you want to donate today, our friends at <a href="https://www.supportukraine.tw/en/">supportukraine.tw</a> have collated a nice list of charities and Ukrainian organizations to donate to.
</p>
<p>What is nice is that for each organization, they provide the source of their information, to prove that it is a legitimate organization, to make sure that the donations really go where it is intended. In places where it might be necessary, they also provide screenshots of forms in Ukrainian anotated in Chinese.
</p>
<p>If you happen to use Facebook, check their group there. They are very active and they are doing great work to drum up support in Taiwan for Ukraine.
</p>
</div>
<div lang="fr">
</div>
</div>

<h3 id="long-term-support-project" class="translated-article">
<span lang="uk"></span>
<span lang="tw">長期支持計劃</span>
<span lang="en">Long term support project</span>
<span lang="fr"></span>
</h3>

<div class="translated-article">
<span lang="uk">Протягом усього березня уряд Китайської Республіки (Тайвань) проводив акцію зі збору коштів на допомогу українським біженцям у Європі. Цей захід зі збору коштів закінчився, і уряд більше не приймає пожертвувань від громадськості. У наступному оновленні ми збираємося надати більш детальну інформацію про це.</span>
<span lang="tw">
整個三月，中華民國（台灣）政府舉行籌款活動，以幫助在歐洲的烏克蘭難民。 本次募款活動已結束，政府不再接受各方捐款。 在未來的更新中，我們將提供有關的更多詳細信息。
</span>
<span lang="en">During the whole month of March, the government of the Republic of China (Taiwan) held a fundraising drive to help Ukrainian refugees in Europe.
This fundraising event is now over, and the government is no longer accepting donations from the public.
In a future update, we are going to provide more details about this.
</span>
<span lang="fr">Pendant tout le mois de mars, le gouvernement de la République de Chine (Taiwan) a organisé une collecte de fonds pour aider les réfugiés ukrainiens en Europe. Cet événement de collecte de fonds est maintenant terminé et le gouvernement n'accepte plus les dons du public. Dans une prochaine mise à jour, nous vous donnerons plus de détails à ce sujet.</span>
</div>


<div class="translated-article">
<span lang="uk">
Навіть після війни Україні потрібно буде відбудовувати всю країну.
Сподіваємося, що тайванський народ знову продемонструє свою щедрість і надасть кошти на відбудову шкіл і лікарень в Україні.
Ми закликаємо уряд Тайваню розпочати довгостроковий збір коштів, заохочуючи людей робити доступні щомісячні пожертви, поки шрами війни не будуть повністю стерті в українських містах.
</span>
<span lang="tw">
即使在戰後，烏克蘭也需要重建整個國家。
我們希望台灣民眾能再次表現出他們的慷慨，提供金錢援助重建烏克蘭的學校和醫院。
我們呼籲台灣政府設立長期的籌款方案，鼓勵民眾每月進行負擔得起的捐款，直到烏克蘭城市的戰爭瘡痍完全消失。
</span>
<span lang="en">
Even after the war, Ukraine will need to rebuild the whole country.
We hope that again the Taiwanese people will demonstrate their generosity and provide funds to rebuild schools and hospitals in Ukraine.
We call on the Taiwanese governement to establish a long term fundraising drive, encouraging people to make affordable monthly donations until the scars of war are completely erased in Ukrainian cities.
</span>
<span lang="fr">
Même après la guerre, l'Ukraine devra reconstruire tout le pays.
Nous espérons qu'une fois de plus le peuple taiwanais fera preuve de générosité et fournira des fonds pour reconstruire des écoles et des hôpitaux en Ukraine.
Nous appelons le gouvernement taïwanais à établir une campagne de collecte de fonds à long terme, encourageant les gens à faire des dons mensuels abordables jusqu'à ce que les cicatrices de la guerre soient complètement effacées dans les villes ukrainiennes.
</span>
</div>

<h3 id="donate-other" class="translated-article">
<span lang="uk"></span>
<span lang="tw">其他捐贈方式</span>
<span lang="en">Other places to donate</span>
<span lang="fr"></span>
</h3>

<div class="translated-article">
<span lang="uk">Тим часом ви можете зробити пожертвування таким організаціям і благодійним організаціям. Ми збираємося вести список добре відомих тайванських організацій або відомих міжнародних організацій, яким тайванці можуть пожертвувати.</span>
<span lang="tw">同時，您可以向以下組織和慈善機構捐款。 我們將保留一份台灣知名組織或知名國際組織的名單，台灣民眾可以向其捐款。</span>
<span lang="en">Meanwhile, you can donate to the following organizations and charities.
We are going to maintain a list of well known Taiwanese organizations or well known international organizations that the Taiwanese people can donate to.
See our suggestions below.
</span>
<span lang="fr">En attendant, vous pouvez faire un don aux organisations et organismes de bienfaisance suivants. Nous allons maintenir une liste d'organisations taïwanaises bien connues ou d'organisations internationales bien connues auxquelles le peuple taïwanais peut faire des dons.</span>
</div>

<div class="translated-article">
<span lang="uk"><strong>Лікарі без кордонів</strong> надають гуманітарну медичну допомогу в зонах бойових дій:<br>
<a href="https://www.doctorswithoutborders.org/what-we-do/countries/ukraine">Médecins Sans Frontieres/Doctors Without Border</a>.
</span>
<span lang="tw">
<strong>無國界醫生</strong>，提供戰地人道醫療援助：<br>
<a href="https://www.doctorswithoutborders.org/what-we-do/countries/ukraine">Médecins Sans Frontieres/Doctors Without Border [英文]</a>.
</span>
<span lang="en">
<strong>Doctors Without Borders</strong> provides humanitarian medical aid in war zones:<br>
<a href="https://www.doctorswithoutborders.org/what-we-do/countries/ukraine">Médecins Sans Frontieres/Doctors Without Border</a>.
</span>
<span lang="fr"><strong>Médecins Sans Frontières</strong> fournit une aide médicale humanitaire dans les zones de guerre :<br>
<a href="https://www.doctorswithoutborders.org/what-we-do/countries/ukraine">Médecins Sans Frontieres/Doctors Without Border</a>.
</span>
</div>



<h2 id="events" class="translated-article">
<span lang="uk">Події</span>
<span lang="tw">活動</span>
<span lang="en">Events</span>
<span lang="fr">Événements</span>
</h2>

<h3 id="may-8-2022-rally" class="translated-article">
<span lang="uk"></span>
<span lang="tw">5月8日烏克蘭邁向勝利集會</span>
<span lang="en">May 8th Ukraine Towards Victory Rally</span>
<span lang="fr"></span>
</h3>

<div class="translated-article">
<div lang="uk">
</div>
<div lang="tw">
<p>「台灣烏克蘭陣線」邀請所有支持烏克蘭的人站出來，向烏克蘭人和世界展示我們的關心。我們相信烏克蘭將在這場不公正戰爭中獲勝！</p>
<p>5月8日是二戰後歐洲迎來和平之日，我們需要向世界展示俄羅斯侵略的殘酷代價，並表明台灣反戰、挺和平民主自由的立場，與受到攻擊的民主同胞站在一起。</p>
<p>我們邀請所有與會者攜帶旗幟、標語或海報。 現場將有媒體報導、烏克蘭籍和台灣籍演說和烏克蘭國歌的演唱。</p>
<p>幾個主要台灣城市（台北、台中、新竹）都有集會，但你可以在任何地方與烏克蘭站在一起。 如果您想在您的城市組織集會，請留言給我們。 或者帶上你的朋友，給我們發出你們與烏克蘭站在一起的照片！</p>
<strong>時間</strong>: 5/8 14:00-15:30<br>
<strong>台北</strong>: 台北自由廣場<br>
<strong>新竹</strong>: 新竹火車站前<br>
<strong>台中</strong>: 台中市民廣場<br>
<strong>活動頁面</strong>: <a href="https://fb.me/e/1BAp0aIAT">烏克蘭邁向勝利集會</a><br>
</p>
</div>

<div lang="en">
<p>Taiwan Stands With Ukraine is inviting everyone who supports Ukraine to come out and show Ukrainians and the world that we care! We believe that Ukraine will prevail in the unjust war that came to its land more than 2 months ago.</p>
<p>On May 8, the day peace came to Europe after WW2, we need to show the world the cruel toll of Russian aggression, and show that Taiwan stands with a fellow democracy under attack.
We invite all attendees to bring a flag or a poster. There will be media coverage, Ukrainian and Taiwanese speakers, and the singing of the Ukrainian national anthem.</p>
<p>There are rallies in several major cities (Taipei, Taichung, Hsinchu), but you can stand with Ukraine anywhere you are. If you want to organize a rally in your city, please message us. Or just come out, bring your friends, stand with Ukraine, and send us photos!</p>

<p>
<strong>Date and time</strong>: 5/8 14:00-15:30<br>
<strong>Taipei</strong>: Taipei Liberty Square<br>
<strong>Hsinchu</strong>: Hsinchu Train Station Square<br>
<strong>Taichung</strong>: Taichung Civic Square<br>
<strong>Web site</strong>: <a href="https://fb.me/e/1BAp0aIAT">May 8th Ukraine Towards Victory Rally</a><br>
</p>
</div>
<div lang="fr">
</div>
</div>


<h3 id="ukraine-art-exhibition" class="translated-article">
<span lang="uk"></span>
<span lang="tw">烏克蘭藝術台中畫展</span>
<span lang="en">Ukraine art exhibition in Taichung</span>
<span lang="fr"></span>
</h3>

<div class="translated-article">
<span lang="uk">Іван Єгоров, український художник, який давно живе в Тайчжуні, Тайвань, надсилає таке запрошення на виставку живопису України:
</span>
<span lang="tw">長期旅居台灣台中的烏克蘭藝術家Ivan Yehorov特邀參加烏克蘭畫展：
</span>
<span lang="en">
Ivan Yehorov, a Ukrainian artist who has long lived in Taichung, Taiwan, is sending the following invitation to an exhibition of paintings of Ukraine:
</span>
<span lang="fr">
Ivan Yehorov, un artiste ukrainien qui a longtemps vécu à Taichung, Taiwan, envoie l'invitation suivante à une exposition de peintures de l'Ukraine:
</span>
</div>

<blockquote class="translated-article">
<span lang="uk">
<strong>Іван Єгоров ～ ЛЮБОВ і МИР</strong><br>
<strong>2022.04.16～05.01</strong><br>
<strong>Центр мистецтв морського порту міста Тайчжун, ТАЙВАНЬ</strong><br>
<br>
<br>
Це виставка, спеціально організована для людей на Тайвані.
<br>
<br>
Завдяки виливу любові та великодушності я зобов’язаний познайомити Україну з людьми, які прагнуть дізнатися більше про Україну. Моя виставка відіграватиме роль «вікна в Україну» в той час, коли Україну розорює Росія.
<br>
<br>Через виставку «Любов і мир» ви побачите Україну, країну з багатою історією та культурою та миролюбною. Ваша гуманітарна допомога зміцнила дух біженців у Європі. Нехай сила любові переможе любов до влади.
<br>
<br>Будь ласка, продовжуйте до StandWithUkraine!
<br>
<br>Мистецтво Івана Єгорова
</span>
<span lang="tw">
<strong>IVAN YEHOROV ～ 愛與和平畫展</strong><br>
<strong>2022.04.16～05.01</strong><br>
<strong>台中市港區藝術中心</strong><br>
<br>
<br>
誠摯地邀請您前來觀賞我特別策畫的「烏克蘭畫展」 !<br>
<br>
此時此刻大家都很關注也想多認識烏克蘭，我們有責任把祖國介紹給慷慨捐獻的台灣同胞，把大家的愛心與烏克蘭人的感恩之心串成一股暖流, 守護人類存活的尊嚴。<br>
<br>
希望我的展覽是 a Window on Ukraine, 帶領您真實認識並珍惜烏克蘭，一個有豐富文化、歷史遺產的愛好和平民族。<br>
<br>
當「權利之愛」love of power大於「愛的力量」 power of love這個世界就陷於紛亂, 但愛的力量匯聚成正義力量時, 可以成就人性共好的精神, 仁慈是人性最美的表現。<br>
<br>
感謝您與烏克蘭同在 !<br>
<br>
P.S. 欲參加開幕式的朋友, 請掃描QR Code登錄, 一起防疫。<br>
<br>
烏克蘭繪畫藝術敬邀<br>
</span>
<span lang="en">
<strong>Ivan Yehorov ～ LOVE and PEACE</strong><br>
<strong>2022.04.16～05.01</strong><br>
<strong>Taichung City Seaport Art Center, TAIWAN</strong><br>
<br>
<br>
This is an exhibition specially organized for the folks in Taiwan.<br>
<br>
Thanks to the outpouring of love and magnanimity, I'm obliged to introduce Ukraine to people who are eager to know more about Ukraine. My exhibition will serve a role as “a Window on Ukraine” during the time Ukraine is savaged by Russia.<br>
<br>
Through the exhibition “Love and Peace” you'll see Ukraine, a country rich in history and culture and peace loving. Your humanitarian assistance has bolstered the spirits of refugees in Europe.Let power of love conquer the love of power.<br>
<br>
Please continue to StandWithUkraine!<br>
<br>
Art of Ivan Yehorov<br>
</span>
<span lang="fr">
<strong>Ivan Yehorov ～ AMOUR et PAIX</strong><br>
<strong>2022.04.16～05.01</strong><br>
<strong>Centre d'art du port maritime de la ville de Taichung, TAÏWAN</strong><br>
<br>
<br>Il s'agit d'une exposition spécialement organisée pour les habitants de Taiwan.
<br>
<br>Grâce à l'effusion d'amour et de générosité, je suis obligé de présenter l'Ukraine aux personnes désireuses d'en savoir plus sur l'Ukraine. Mon exposition servira de « fenêtre sur l'Ukraine » à l'époque où l'Ukraine est ravagée par la Russie.
<br>
<br>À travers l'exposition « Amour et Paix », vous découvrirez l'Ukraine, un pays riche d'histoire et de culture et épris de paix. Votre aide humanitaire a remonté le moral des réfugiés en Europe. Laissez le pouvoir de l'amour conquérir l'amour du pouvoir.
<br>
<br>Veuillez continuer soutenir l'Ukraine: StandWithUkraine!
<br>
<br>L'art d'Ivan Yehorov
</span>
</blockquote>

<h2 id="participate" class="translated-article">
<span lang="uk">Беріть участь</span>
<span lang="tw">參加</span>
<span lang="en">Participate</span>
<span lang="fr">Participer</span>
</h2>

<div class="translated-article">
<div lang="uk">
<strong>Внесіть свої унікальні навички в цей гуманітарний проект.</strong>
Усі дії користувачів обробляються через наш <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">проект TaiwanUkraine Codeberg</a>. <a href="https://codeberg.org/user/sing_up">Зареєструйтеся</a> на Codeberg і повідомте нам, що можна зробити для розвитку цього проекту та покращення будь-якого його аспекту.
</div>
<div lang="tw">
<strong>為這個人道主義項目貢獻你的獨特技能。</strong>
所有用戶活動均通過我們的 <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">TaiwanUkraine 計畫在Codeberg</a> 處理。 在 Codeberg <a href="https://codeberg.org/user/sing_up">註冊</a>，讓我們知道可以做些什麼來開發這個項目並改進它的任何方面。
</div>
<div lang="en">
<strong>Contribute your unique skills to this humanitarian project.</strong>
All user activities are handled via our <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">TaiwanUkraine project at Codeberg</a>. <a href="https://codeberg.org/user/sing_up">Register</a> at Codeberg, and let us know what can be done to develop this project and improve any aspect of it.
</div>
<div lang="fr">
<strong>Mettez vos compétences uniques au service de ce projet humanitaire.</strong>
Toutes les activités des utilisateurs sont gérées via notre <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">projet TaiwanUkraine à Codeberg</a>. <a href="https://codeberg.org/user/sing_up">Inscrivez-vous</a> à Codeberg et faites-nous savoir ce qui peut être fait pour développer ce projet et en améliorer n'importe quel aspect.
</div>
</div>



<h2 id="development" class="translated-article">
<span lang="uk">Розробка веб-сайту</span>
<span lang="tw">本網站開發</span>
<span lang="en">Web site development</span>
<span lang="fr">Dévelopment du site web</span>
</h2>

<div class="translated-article">
<span lang="uk">Цей веб-сайт зараз розробляється і приймати внески користувачів.</span>
<span lang="tw">該網站目前正在開發中，並接受用戶的建議，以及協助網站的開發。</span>
<span lang="en">This web site is currently being developed and is accepting user contributions.</span>
<span lang="fr">Ce site web est en cours de construction et accepte vos contributions.</span>
</div>

<div class="translated-article">
<span lang="uk">
<strong>
Веб-сайт стане тим, чим ми з нього спільно зробимо.
</strong>
Пріоритетним є надання інформації тайванцям, як робити пожертви для України, як допомогти Україні. Крім того, у довгостроковій перспективі сайт може надати практичну інформацію будь-якій людині, яка має зв’язок з двома країнами, наприклад, студентам, тайванцям в Україні, українцям на Тайвані тощо...
</span>
<span lang="tw">
<strong>
這個網站將會是我們共同創造的。
</strong>
當務之急是向台灣民眾提供信息，如何為烏克蘭捐款，如何幫助烏克蘭。 此外，從長遠來看，這個網站可以為任何與兩國有聯繫的人提供實用信息，如在烏克蘭的學生和台灣人、在台灣的烏克蘭人等。
</span>
<span lang="en">
<strong>
The web site will become whatever we collectively make of it.
</strong>
The priority is to provide information to Taiwanese people how to donate for Ukraine, how to help Ukraine.

In addition, in the long term, the site can provide practical information to any person who has a connection with the two countries, like students, Taiwanese people in Ukraine, Ukrainian people in Taiwan, etc...
</span>
<span lang="fr">
<strong>
Le site Web deviendra ce que nous en ferons collectivement.
</strong>
La priorité est de fournir des informations aux Taïwanais sur la manière de faire un don pour l'Ukraine, sur la manière d'aider l'Ukraine. De plus, à terme, le site pourra fournir des informations pratiques à toute personne ayant un lien avec les deux pays, comme les étudiants, les Taïwanais en Ukraine, les Ukrainiens à Taïwan, etc...
</span>
</div>

<div class="translated-article">
<span lang="uk">
Весь код для цього веб-сайту розміщено в <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">проекті TaiwanUkraine в Codeberg</a>. Ви можете зареєструватися на Codeberg і подати пропозиції щодо покращення цього веб-сайту з точки зору презентації, функцій та вмісту. Якщо у вас є технічні знання, ви навіть можете допомогти розробити код (простий HTML), надіславши запити на витяг.
</span>
<span lang="tw">
該網站的全部代碼託管在<a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues"> Codeberg 的 TaiwanUkraine 計畫</a>中。 您可以在 Codeberg 註冊並提交有關如何在演示、功能和內容方面改進此網站的建議。 如果您有技術知識，您甚至可以通過提交拉取請求來幫助開發代碼（簡單的 HTML）。
</span>
<span lang="en">
The whole code for this web site is hosted at the <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">TaiwanUkraine project at Codeberg</a>.
You can register at Codeberg and submit suggestions on how to improve this web site, in terms of presentation, features, and content.
If you have the technical knowledge, you can even help develop the code (simple HTML) by submitting pull requests.
</span>
<span lang="fr">
L'ensemble du code de ce site Web est hébergé sur le <a href="https://codeberg.org/TaiwanDemocracy/TaiwanUkraine/issues">projet TaiwanUkraine à Codeberg</a>. Vous pouvez vous inscrire sur Codeberg et soumettre des suggestions sur la façon d'améliorer ce site Web, en termes de présentation, de fonctionnalités et de contenu. Si vous avez les connaissances techniques, vous pouvez même aider à développer le code (HTML simple) en soumettant des pull requests.
</span>
</div>


<style>

body {
	padding: 1em;
}
.translated-article {
	display: grid;
	grid-template-columns: 1fr;
	column-gap: 1em;
}

div.translated-article,
p.translated-article {
	margin: 0;
}

.translated-article div ,
.translated-article span {
	padding: 0.8em;
	border-radius: 0.5em;
}

div:lang(uk) ,
span:lang(uk) {
	display: none;
	background: hsla(  3, 85%, 96%, 1);
}
div:lang(tw) ,
span:lang(tw) {
	background: hsla(212, 88%, 99%, 0.5);
	border-left:  3px solid hsla(212, 100%, 50%, 1);
	border-right: 3px solid hsla(212, 100%, 50%, 1);
}
div:lang(en) ,
span:lang(en) {
	background: hsla( 55, 88%, 99%, 0.5);
	border-left:  3px solid hsla( 55, 100%, 50%, 1);
	border-right: 3px solid hsla( 55, 100%, 50%, 1);
}
div:lang(fr) ,
span:lang(fr) {
	display: none;
	background: hsla(126, 88%, 96%, 1);
}

h1 span:lang(uk) {
	border-top: 10px solid hsla(  3, 85%, 50%, 1);
	color:                 hsla(  3, 85%, 50%, 1);
	background-color: black;
	text-align: center;
}
h1 span:lang(tw) {
	border-top: 10px solid hsla(212, 88%, 50%, 1);
	color:                 hsla(212, 88%, 50%, 1);
	background-color: black;
	text-align: center;
}
h1 span:lang(en) {
	border-top: 10px solid hsla( 55, 88%, 50%, 1);
	color:                 hsla( 55, 88%, 50%, 1);
	background-color: black;
	text-align: center;
}
h1 span:lang(fr) {
	border-top: 10px solid hsla(126, 88%, 50%, 1);
	color:                 hsla(126, 88%, 50%, 1);
	background-color: black;
	text-align: center;
}


h2 span:lang(uk) {
	border: none;
	border-top: 5px solid hsla(  3,  85%, 50%, 1);
	color:                hsla(  3, 100%, 50%, 1);
	text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
h2 span:lang(tw) {
	border: none;
	border-top: 5px solid hsla(212,  88%, 50%, 1);
	color:                hsla(212, 100%, 50%, 1);
	text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
h2 span:lang(en) {
	border: none;
	border-top: 5px solid hsla( 55,  88%, 50%, 1);
	color:                hsla( 55, 100%, 50%, 1);
	text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
h2 span:lang(fr) {
	border: none;
	border-top: 5px solid hsla(126,  88%, 50%, 1);
	color:                hsla(126, 100%, 50%, 1);
	text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}


h3 span:lang(uk) {
	border: none;
	border-top: 1px solid hsla(  3, 85%, 50%, 1);
}
h3 span:lang(tw) {
	border: none;
	border-top: 1px solid hsla(212, 88%, 50%, 1);
}
h3 span:lang(en) {
	border: none;
	border-top: 1px solid hsla( 55, 88%, 50%, 1);
}
h3 span:lang(fr) {
	border: none;
	border-top: 1px solid hsla(126, 88%, 50%, 1);
}

/* <blockquote> */

blockquote span:lang(uk) {
	border: 1px solid hsla(  3, 85%, 50%, 1);
}
blockquote span:lang(tw) {
	border: 1px solid hsla(212, 88%, 50%, 1);
}
blockquote span:lang(en) {
	border: 1px solid hsla( 55, 88%, 50%, 1);
}
blockquote span:lang(fr) {
	border: 1px solid hsla(126, 88%, 50%, 1);
}

blockquote {
	color: hsla(110, 0%, 20%, 1);
	font-style: italic;
}


/* <img> */

img.banner {
	display: grid;
	grid-template-columns: 1fr;
	width: 100%;
	object-fit: cover;
	max-height: 50vh;
}

/* <strong> */

.translated-article strong {
	background-color: hsla(0, 0%, 25%, 1);
	padding-right: 0.5em;
	padding-left: 0.5em;
}

.translated-article *:lang(tw) strong {
	color: hsla(212, 88%, 70%, 1);
}
.translated-article *:lang(en) strong {
	color: hsla( 55, 88%, 50%, 1);
}
.translated-article strong.red {
	color: hsla( 0, 100%, 70%, 1);
}

@media (min-width:  750px) {
	.translated-article {
		grid-template-columns: 1fr 1fr;
	}
}
@media (min-width:  1200px) {
	.translated-article {
		grid-template-columns: 1fr 1fr;
	}
}
</style>
EOF;
include('template/page.php');
