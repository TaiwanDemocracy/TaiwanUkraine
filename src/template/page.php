<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>台灣 - Україна</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta name='viewport'   content='width=device-width, initial-scale=1.0, maximum-scale=1.0' />
	<link rel="stylesheet" href="semantic/semantic.min.css" type="text/css" media="all" />
	<script type="text/javascript" src="semantic/semantic.min.js"></script>

	<meta property="og:title"  content="台灣-Україна - Peace and Democracy 民主與和平">
	<meta name="twitter:title" content="台灣-Україна - Peace and Democracy 民主與和平">

	<meta name="description"         content="Taiwan and Ukraine for Peace and Democracy. 台灣人支持民主與和平、支持烏克蘭：愛與團結的力量！">
	<meta property="og:description"  content="Taiwan and Ukraine for Peace and Democracy. 台灣人支持民主與和平、支持烏克蘭：愛與團結的力量！">
	<meta name="twitter:description" content="Taiwan and Ukraine for Peace and Democracy. 台灣人支持民主與和平、支持烏克蘭：愛與團結的力量！">

	<meta property="og:image"  content="https://ukraine-taiwan.tw/img/sewing-ukraine-wall-preview.jpg">
	<meta name="twitter:image" content="https://ukraine-taiwan.tw/img/sewing-ukraine-wall-preview.jpg">
	<meta name="twitter:card"  content="summary_large_image">
</head>

<body id="pagetop" class="">
<?php echo $body; ?>



</body>
</html>
